%%

%unicode

%class Lexer
%line
%column
%standalone
%{


%}

/* required rules */
letter = [a-zA-Z]
digit = [0-9]

/* identifiers */
Identifier = {letter}({letter} | {digit})*

/* main character classes */
LineTerminator = \r|\n|\r\n
InputCharacter = [^\r\n]

WhiteSpace = {LineTerminator} | [ \t\f]

/* comments */
Comment = {PcatComment}

PcatComment = "(*" {InputCharacter}* "*)" {LineTerminator}?
PcatErrorComment = "(*" {InputCharacter}* {LineTerminator}?

/* integer literals */
IntegerLiteral    = {digit}+
RealLiteral       = {digit}+[.]{digit}*

/* string and character literals */
StringCharacter = [^\r\n\"\\]

CommentError = {PcatErrorComment}
IdentifierError = {digit}|[_]{Identifier}
%state STRING, CHARLITERAL

%%

<YYINITIAL> {

/* keywords */
"AND"                          { System.out.println("AND"); }
"ARRAY"                        { System.out.println("ARRAY"); }
"BEGIN"                        { System.out.println("BEGIN"); }
"BY"                           { System.out.println("BY"); }
"DIV"                          { System.out.println("DIV"); }
"DO"                           { System.out.println("DO"); }
"ELSEIF"                       { System.out.println("ELSEIF"); }
"END"                          { System.out.println("END"); }
"EXIT"                         { System.out.println("EXIT"); }
"IS"                           { System.out.println("IS"); }
"LOOP"                         { System.out.println("LOOP"); }
"MOD"                          { System.out.println("MOD"); }
"OF"                           { System.out.println("OF"); }
"PROCEDURE"                    { System.out.println("PROCEDURE"); }
"PROGRAM"                      { System.out.println("PROGRAM"); }
"READ"                         { System.out.println("READ"); }
"RECORD"                       { System.out.println("RECORD"); }
"THEN"                         { System.out.println("THEN"); }
"TO"                           { System.out.println("TO"); }
"TYPE"                         { System.out.println("TYPE"); }
"VAR"                          { System.out.println("VAR"); }
"WRITE"                        { System.out.println("WRITE"); }
"DO"                           { System.out.println("DO"); }
"ELSE"                         { System.out.println("ELSE"); }
"FOR"                          { System.out.println("FOR"); }
"IF"                           { System.out.println("IF"); }
"RETURN"                       { System.out.println("RETURN"); }
"WHILE"                        { System.out.println("WHILE"); }


/* numeric literals */
{IntegerLiteral}               { System.out.println("Integer"); }
{RealLiteral}                  { System.out.println("Real"); }

/* comments */
{Comment}                      { /* ignore */ }

/* error */
{CommentError}                 { throw new RuntimeException("Error at line " + yyline +
                                        ", column " + yycolumn + ", Dangling comment. Exiting"); }

/* whitespace */
{WhiteSpace}                   { /* ignore */ }

/* separators */ //Done
"("                            { System.out.println("("); }
")"                            { System.out.println(")"); }
"{"                            { System.out.println("{"); }
"}"                            { System.out.println("}"); }
"["                            { System.out.println("["); }
"]"                            { System.out.println("]"); }
";"                            { System.out.println(";"); }
","                            { System.out.println(","); }
"."                            { System.out.println("."); }
":"                            { System.out.println(":"); }

/* operators */ //Done
"="                            { System.out.println("="); }
">"                            { System.out.println(">"); }
"<"                            { System.out.println("<"); }
"<>"                           { System.out.println("<>"); }
"<="                           { System.out.println("<="); }
">="                           { System.out.println(">="); }
":="                           { System.out.println(":="); }
"+"                            { System.out.println("+"); }
"-"                            { System.out.println("-"); }
"*"                            { System.out.println("*"); }
"/"                            { System.out.println("/"); }
"AND"                          { System.out.println("AND"); }
"OR"                           { System.out.println("OR"); }
"MOD"                          { System.out.println("MOD"); }

/* identifiers */
{Identifier}                   { System.out.println("Variable"); }

{IdentifierError}              { System.out.println(new RuntimeException("Error at line " + yyline
                                    + ",column " + yycolumn + ", Identifier "
                                    + yytext() + " must begin with a letter. Skipping")); }

/* string literal */
\"                             { System.out.println("String begins"); }

/* character literal */
\'                             { System.out.println("Char begins"); }

}

<STRING> {
\"                             { System.out.println("STRING type"); }

{StringCharacter}+             { System.out.println("string character"); }

/* escape sequences */
"\\b"                          /* escape sequences */
"\\b"                          { System.out.println("\\\\b"); }
"\\t"                          { System.out.println("\\\\t"); }
"\\n"                          { System.out.println("\\\\n"); }
"\\f"                          { System.out.println("\\\\f"); }
"\\r"                          { System.out.println("\\\\r"); }
"\\\""                         { System.out.println("\\\\"); }
"\\'"                          { System.out.println("\\\\'"); }
"\\\\"                         { System.out.println("\\\\"); }

/* error cases */
\\.                            { System.out.println(new RuntimeException("Illegal escape sequence \""+yytext()+"\"")); }
{LineTerminator}               { System.out.println(new RuntimeException("Error at line " + yyline
                                    + ", column " + yycolumn +
                                    ", Unterminated string. Skipping")); }
}

<CHARLITERAL> {
/* error cases */
\\.                            { System.out.println(new RuntimeException("Illegal escape sequence \""+yytext()+"\"")); }
{LineTerminator}               { System.out.println(new RuntimeException("Unterminated character literal at end of line")); }
}

/* error fallback */
[^]                              { System.out.println(new RuntimeException("Unrecognized Symbol \""+yytext()+
                                    "\" at line "+yyline+", column "+yycolumn+ ". Skipping")); }